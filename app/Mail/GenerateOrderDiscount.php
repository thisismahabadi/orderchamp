<?php

declare(strict_types=1);

namespace App\Mail;

use App\Models\Discount;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

final class GenerateOrderDiscount extends Mailable
{
    use Queueable, SerializesModels;

    private Discount $discount;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Discount $discount)
    {
        $this->discount = $discount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('order.discount', ['discount' => $this->discount]);
    }
}
