<?php

declare(strict_types=1);

namespace App\Services\OrderItem;

use App\Models\Order;
use App\Models\Product;
use App\Repositories\Product\ProductRepository;
use Exception;

final class StoreOrderItemService
{
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function run(array $cartItems, Order $order): void
    {
        foreach ($cartItems as $productId => $quantity) {
            if ($this->productRepository->getInStockProductQuantity($productId) >= $quantity) {
                $order->items()->create(['product_id' => $productId, 'quantity' => $quantity]);

                Product::query()
                    ->where('id', $productId)
                    ->decrement('quantity', $quantity);
            } else {
                throw new Exception('The product has less quantity than your request, please review your order.', 400);
            }
        }
    }
}
