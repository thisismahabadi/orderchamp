<?php

declare(strict_types=1);

namespace App\Services\CartItem;

use App\Models\CartItem;
use App\Repositories\Product\ProductRepository;
use Illuminate\Http\Request;
use Exception;

final class StoreCartService
{
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function run(Request $request): CartItem
    {
        $inStockProductQuantity = $this->productRepository->getInStockProductQuantity($request->input('product_id'));

        if (!$inStockProductQuantity || $inStockProductQuantity < $request->input('quantity')) {
            throw new Exception('The product is not in stock.', 400);
        }

        return CartItem::query()
            ->updateOrCreate([
                    'user_id' => $request->user()->id,
                    'product_id' => $request->input('product_id'),
                ], [
                    'user_id' => $request->user()->id,
                    'product_id' => $request->input('product_id'),
                    'quantity' => $request->input('quantity'),
            ]);
    }
}
