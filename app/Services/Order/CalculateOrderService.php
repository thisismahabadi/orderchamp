<?php

declare(strict_types=1);

namespace App\Services\Order;

use App\Models\OrderItem;
use App\Models\Product;
use App\Repositories\Discount\DiscountRepository;

final class CalculateOrderService
{
    private DiscountRepository $discountRepository;

    public function __construct(DiscountRepository $discountRepository)
    {
        $this->discountRepository = $discountRepository;
    }

    public function calculateOrderCost(int $orderId): float
    {
        $orderItems = OrderItem::query()
            ->where('order_id', $orderId)
            ->pluck('quantity', 'product_id')
            ->toArray();

        $cost = 0;

        foreach ($orderItems as $productId => $quantity) {
            $productPrice = Product::query()
                ->where('id', $productId)
                ->pluck('price')
                ->first();

            $cost += $productPrice * $quantity;
        }

        return $cost;
    }

    public function calculateOrderWithDiscount(float $orderAmount, int $userId, ?string $discountCode): float
    {
        if ($discountCode) {
            $discountAmount = $this->discountRepository->getDiscountAmountByCodeAndUser($userId, $discountCode);

            $orderAmount -= $discountAmount;
        }

        return $orderAmount;
    }

    public function run(int $orderId, int $userId, ?string $discountCode): float
    {
        $orderAmount = $this->calculateOrderCost($orderId);

        return $this->calculateOrderWithDiscount($orderAmount, $userId, $discountCode);
    }
}
