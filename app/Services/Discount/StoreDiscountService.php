<?php

declare(strict_types=1);

namespace App\Services\Discount;

use App\Models\Discount;
use Illuminate\Support\Str;

final class StoreDiscountService
{
    public function run(int $userId, int $orderId): Discount
    {
        return Discount::query()
            ->create([
                'user_id' => $userId,
                'code' => Str::random(10),
                'description' => 'Discount because of your last order: ' . $orderId,
                'amount' => 5,
                'is_active' => true,
        ]);
    }
}
