<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Http\Resources\OrderResource;
use App\Services\Order\StoreOrderService;

final class OrderController extends Controller
{
    public function store(OrderRequest $request, StoreOrderService $service): OrderResource
    {
        $order = $service->run($request);

        return OrderResource::make($order);
    }
}
