<?php

declare(strict_types=1);

namespace App\Repositories\CartItem;

use App\Models\CartItem;

final class CartItemRepository
{
    private CartItem $cartItem;

    public function __construct(CartItem $cartItem)
    {
        $this->cartItem = $cartItem;
    }

    public function getUserItems(int $userId): array
    {
        return $this->cartItem
            ->query()
            ->where('user_id', $userId)
            ->pluck('quantity', 'product_id')
            ->toArray();
    }
}
