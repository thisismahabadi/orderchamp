<?php

declare(strict_types=1);

namespace App\Repositories\Discount;

use App\Models\Discount;

final class DiscountRepository
{
    private Discount $discount;

    public function __construct(Discount $discount)
    {
        $this->discount = $discount;
    }

    public function getDiscountIdByCodeAndUser(int $userId, string $discountCode): ?int
    {
        return $this->discount
            ->query()
            ->where('user_id', $userId)
            ->where('code', $discountCode)
            ->pluck('id')
            ->first();
    }

    public function getDiscountAmountByCodeAndUser(int $userId, string $discountCode): ?float
    {
        return $this->discount
            ->query()
            ->where('user_id', $userId)
            ->where('code', $discountCode)
            ->where('is_active', true)
            ->pluck('amount')
            ->first();
    }
}
