<?php

declare(strict_types=1);

namespace App\Repositories\Product;

use App\Models\Product;

final class ProductRepository
{
    private Product $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getInStockProductQuantity(int $productId): int
    {
        return $this->product
            ->query()
            ->where('id', $productId)
            ->where('quantity', '>', 0)
            ->pluck('quantity')
            ->first() ?? 0;
    }
}
