<?php

declare(strict_types=1);

namespace Tests\Feature\Cart;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

final class StoreCartTest extends TestCase
{
    use RefreshDatabase;

    public function test_unauthenticated(): void
    {
        $this->postJson('api/v1/carts')
            ->assertUnauthorized()
            ->assertExactJson([
                'message' => 'Unauthenticated.',
            ]);
    }

    public function test_without_body_parameters(): void
    {
        $user = User::query()
            ->create([
                'email' => 'mohammad.mahabadi@hotmail.com',
                'password' => Hash::make('123456'),
                'address' => 'Utrecht',
                'mobile_number' => '+316593591',
                'name' => 'Mohammad',
            ]);

        $this->actingAs($user);

        $this->postJson('api/v1/carts')
            ->assertUnprocessable()
            ->assertExactJson([
                'errors' => [
                    'product_id' => [
                        'The product id field is required.',
                    ],
                    'quantity' => [
                        'The quantity field is required.',
                    ],
                ],
                'message' => 'The given data was invalid.',
            ]);
    }

    public function test_with_invalid_body_data_types(): void
    {
        $user = User::query()
            ->create([
                'email' => 'mohammad.mahabadi@hotmail.com',
                'password' => Hash::make('123456'),
                'address' => 'Utrecht',
                'mobile_number' => '+316593591',
                'name' => 'Mohammad',
            ]);

        $this->actingAs($user);

        $this->postJson('api/v1/carts', [
            'product_id' => 1,
            'quantity' => [2],
        ])
            ->assertUnprocessable()
            ->assertExactJson([
                'errors' => [
                    'product_id' => [
                        'The selected product id is invalid.',
                    ],
                    'quantity' => [
                        'The quantity must be an integer.',
                        'The quantity must be a number.',
                    ],
                ],
                'message' => 'The given data was invalid.',
            ]);
    }

    public function test_with_invalid_quantity(): void
    {
        $user = User::query()
            ->create([
                'email' => 'mohammad.mahabadi@hotmail.com',
                'password' => Hash::make('123456'),
                'address' => 'Utrecht',
                'mobile_number' => '+316593591',
                'name' => 'Mohammad',
            ]);

        $product = Product::query()
            ->create([
                'name' => 'laptop',
                'description' => 'this is a laptop.',
                'quantity' => 1,
                'price' => 400,
            ]);

        $this->actingAs($user);

        $this->postJson('api/v1/carts', [
            'product_id' => $product->id,
            'quantity' => 2,
        ])
            ->assertStatus(500)
            ->assertJsonFragment([
                'message' => 'The product is not in stock.',
            ]);
    }

    public function test_with_valid_body_parameters(): void
    {
        $user = User::query()
            ->create([
                'email' => 'mohammad.mahabadi@hotmail.com',
                'password' => Hash::make('123456'),
                'address' => 'Utrecht',
                'mobile_number' => '+316593591',
                'name' => 'Mohammad',
            ]);

        $product = Product::query()
            ->create([
                'name' => 'laptop',
                'description' => 'this is a laptop.',
                'quantity' => 5,
                'price' => 400,
            ]);

        $this->actingAs($user);

        $this->postJson('api/v1/carts', [
            'product_id' => $product->id,
            'quantity' => 1,
        ])
            ->assertCreated()
            ->assertJson([
                'data' => [
                    'product_id' => $product->id,
                    'quantity' => 1,
                    'user_id' => $user->id,
                ],
            ]);
    }
}
