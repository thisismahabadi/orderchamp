<?php

declare(strict_types=1);

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

final class SignInTest extends TestCase
{
    use RefreshDatabase;

    public function test_without_body_parameters(): void
    {
        $this->postJson('api/v1/user/login')
            ->assertUnprocessable()
            ->assertExactJson([
                'errors' => [
                    'email' => [
                        'The email field is required.',
                    ],
                    'password' => [
                        'The password field is required.',
                    ],
                ],
                'message' => 'The given data was invalid.',
            ]);
    }

    public function test_with_invalid_body_parameters(): void
    {
        $this->postJson('api/v1/user/login', [
            'email' => 'lol@bol.com',
            'password' => 1234,
        ])
            ->assertUnprocessable()
            ->assertExactJson([
                'errors' => [
                    'email' => [
                        'The selected email is invalid.',
                    ],
                    'password' => [
                        'The password must be at least 5 characters.',
                    ],
                ],
                'message' => 'The given data was invalid.',
            ]);
    }

    public function test_with_valid_body_parameters(): void
    {
        User::query()
            ->create([
                'email' => 'mohammad.mahabadi@hotmail.com',
                'password' => Hash::make('123456'),
                'address' => 'Utrecht',
                'mobile_number' => '+316593591',
                'name' => 'Mohammad',
            ]);

        $this->postJson('api/v1/user/login', [
            'email' => 'mohammad.mahabadi@hotmail.com',
            'password' => 123456,
        ])
            ->assertOk();
    }

    public function test_with_wrong_credentials(): void
    {
        User::query()
            ->create([
                'email' => 'mohammad.mahabadi@hotmail.com',
                'password' => Hash::make('123456'),
                'address' => 'Utrecht',
                'mobile_number' => '+316593591',
                'name' => 'Mohammad',
            ]);

        $this->postJson('api/v1/user/login', [
            'email' => 'mohammad.mahabadi@hotmail.com',
            'password' => 1234567,
        ])
            ->assertStatus(500)
            ->assertJsonFragment([
                'message' => 'Credentials are wrong!',
            ]);
    }
}
