<?php

declare(strict_types=1);

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class SignUpTest extends TestCase
{
    use RefreshDatabase;

    public function test_without_body_inputs(): void
    {
        $this->postJson('api/v1/user/register')
            ->assertUnprocessable()
            ->assertExactJson([
                'errors' => [
                    'address' => [
                        'The address field is required.',
                    ],
                    'email' => [
                        'The email field is required.',
                    ],
                    'mobile_number' => [
                        'The mobile number field is required.',
                    ],
                    'name' => [
                        'The name field is required.',
                    ],
                    'password' => [
                        'The password field is required.',
                    ],
                ],
                'message' => 'The given data was invalid.',
            ]);
    }

    public function test_with_valid_parameters(): void
    {
        $this->postJson('api/v1/user/register', [
            'address' => 'Utrecht',
            'email' => 'mohammad.mahabadi@hotmail.com',
            'mobile_number' => '+316593591',
            'name' => 'Mohammad',
            'password' => 123456,
        ])
            ->assertCreated();

        $this->assertDatabaseHas('users', [
            'address' => 'Utrecht',
            'email' => 'mohammad.mahabadi@hotmail.com',
            'mobile_number' => '+316593591',
            'name' => 'Mohammad',
        ]);
    }

    public function test_with_invalid_validation_parameters(): void
    {
        $this->postJson('api/v1/user/register', [
            'address' => ['Utrecht'],
            'email' => ['mohammad.mahabadi@hotmail.com'],
            'mobile_number' => ['+316593591'],
            'name' => [123],
            'password' => 1234,
        ])
            ->assertUnprocessable()
            ->assertExactJson([
                'errors' => [
                    'address' => [
                        'The address must be a string.',
                    ],
                    'email' => [
                        'The email must be a string.',
                        'The email must be a valid email address.',
                    ],
                    'mobile_number' => [
                        'The mobile number must be a string.',
                    ],
                    'name' => [
                        'The name must be a string.',
                    ],
                    'password' => [
                        'The password must be at least 5 characters.',
                    ],
                ],
                'message' => 'The given data was invalid.',
            ]);

        $this->assertDatabaseMissing('users', [
            'address' => 'Utrecht',
            'email' => 'mohammad.mahabadi@hotmail.com',
            'mobile_number' => '+316593591',
            'name' => 'Mohammad',
        ]);
    }
}
