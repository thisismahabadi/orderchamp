<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(static function (): void {
    Route::prefix('user')->controller(AuthController::class)
        ->group(static function (): void {
            Route::post('register', 'register');

            Route::post('login', 'login');
        });

    Route::middleware('auth:sanctum')->group(static function (): void {
        Route::get('user', [UserController::class, 'get']);

        Route::post('carts', [CartController::class, 'store']);

        Route::post('orders', [OrderController::class, 'store']);
    });
});
